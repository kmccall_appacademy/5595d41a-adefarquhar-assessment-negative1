#require 'byebug'
# Given an array of unique integers ordered from least to greatest, write a
# method that returns an array of the integers that are needed to
# fill in the consecutive set.

def missing_numbers(nums)
  missing = []
  nums.each_index do |idx|
    break if nums[idx] == nums[-1]
    if nums[idx] != nums[idx+1]
      missing.push((nums[idx]+1...nums[idx+1]).to_a)
    end
  end

  missing.flatten
end

# Write a method that given a string representation of a binary number will
# return that binary number in base 10.
#
# To convert from binary to base 10, we take the sum of each digit multiplied by
# two raised to the power of its index. For example:
#   1001 = [ 1 * 2^3 ] + [ 0 * 2^2 ] + [ 0 * 2^1 ] + [ 1 * 2^0 ] = 9
#
# You may NOT use the Ruby String class's built in base conversion method.

def base2to10(binary)
  digits = binary.chars
  len = digits.length
  sum = 0
  digits.each_index do |idx|
    sum += digits[idx].to_i*(2**(len-1-idx))
  end
  sum
end

class Hash

  # Hash#select passes each key-value pair of a hash to the block (the proc
  # accepts two arguments: a key and a value). Key-value pairs that return true
  # when passed to the block are added to a new hash. Key-value pairs that return
  # false are not. Hash#select then returns the new hash.
  #
  # Write your own Hash#select method by monkey patching the Hash class. Your
  # Hash#my_select method should have the functionailty of Hash#select described
  # above. Do not use Hash#select in your method.

  def my_select(&prc)
    selected = Hash.new
    self.each do |key, val|
      selected[key] = val if prc.call(key, val)
    end
    selected
  end

end

class Hash

  # Hash#merge takes a proc that accepts three arguments: a key and the two
  # corresponding values in the hashes being merged. Hash#merge then sets that
  # key to the return value of the proc in a new hash. If no proc is given,
  # Hash#merge simply merges the two hashes.
  #
  # Write a method with the functionality of Hash#merge. Your Hash#my_merge method
  # should optionally take a proc as an argument and return a new hash. If a proc
  # is not given, your method should provide default merging behavior. Do not use
  # Hash#merge in your method.

  def my_merge(hash, &prc)
    if prc == nil
      hash.each do |key, val|
        self[key] = val
      end
      return self
    end

    merged = Hash.new
    hash.each do |key, val|
      merged[key] = prc.call(key, self[key], val)
    end
    merged
  end

end

# The Lucas series is a sequence of integers that extends infinitely in both
# positive and negative directions.
#
# The first two numbers in the Lucas series are 2 and 1. A Lucas number can
# be calculated as the sum of the previous two numbers in the sequence.
# A Lucas number can also be calculated as the difference between the next
# two numbers in the sequence.
#
# All numbers in the Lucas series are indexed. The number 2 is
# located at index 0. The number 1 is located at index 1, and the number -1 is
# located at index -1. You might find the chart below helpful:
#
# Lucas series: ...-11,  7,  -4,  3,  -1,  2,  1,  3,  4,  7,  11...
# Indices:      ... -5, -4,  -3, -2,  -1,  0,  1,  2,  3,  4,  5...
#
# Write a method that takes an input N and returns the number at the Nth index
# position of the Lucas series.

def lucas_numbers(n)
  return 2 if n == 0
  return 1 if n == 1

  num1 = 2
  num2 = 1
  num3 = nil

  if n > 1
    (2..n).each do |idx|
      num3 = num1 + num2
      num1 = num2
      num2 = num3
    end
  elsif n < 0
    (n..-1).each do |idx|
      num3 = num2 - num1
      num2 = num1
      num1 = num3
    end
  end

  num3
end

# A palindrome is a word or sequence of words that reads the same backwards as
# forwards. Write a method that returns the longest palindrome in a given
# string. If there is no palindrome longer than two letters, return false.

def longest_palindrome(string)
  longest = 0
  string = string.chars
  (2...string.length).each do |len|
    string.each_index do |idx|
      break if idx+len > string.length
      sub_str = string[idx, len]
      if palindrome?(sub_str) && sub_str.length > longest
        longest = sub_str.length
      end
    end
  end

  longest > 2 ? longest : false
end

def palindrome?(string)
  true if string == string.reverse
end
